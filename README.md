# Backbase Mobile Assignment R&D

Challenge to parse large json file of city records around ~19MB and search/ filter record out of it.


## Getting Started
These instructions will help you to run the project on Android Studio and create executable to install on device.
With the help of below steps you can run the test cases as well to check search filter result without running application.

## Prerequisites

### Software

* Windows / Mac / Linux operating system
* Java
* [Android Studio](https://developer.android.com/studio/) installed on system
* [Git Client](https://git-scm.com/download/) installed on system
* Android Device or Emulator on system to test the application.


### Hardware
* Emulator / Device (min 1.5GB RAM) for decent performance
* PC / Laptop (min 4GB RAM)


## Application Permission
```
No Permission required

```

## Component Used
* Activity
* IntentService
* Broadcast Receiver (Local)
* <del>No Database</del>
* <del>No Shared Preferences</del>
* Google Map


## Application Setup

* Extract BBAndroidAssignment.zip
* Open Android Studio -> File -> Open
* Select extracted project folder -> Open
* copy "cities.json" file and paste it to <b>ProjectFolder->app->src->main->assets</b>
* Build Project
* Android Studio -> Tools -> Android ->AVD Manager
* Create Emulator
* Run -> Run App


## Task
Your task is to:
* Display this of cities on a scrollable list in alphabetic order (city first, country after)
  * "Denver, US" should appear before, "Sydney, Australia"
  * "Anaheim, US" should appear before "Denver, US"
* Be able to filter the results by a given prefix string, over the city.
* Selecting a city will show a map centered on the coordinates associated with the city.
* Optimize for fast searches, loading time of the app is not so important

### Example
We define a prefix string as: a substring that matches the initial characters of the target string. For instance, assume the following entries:

>Alabama, US
>
>Albuquerque, US
>
>Anaheim, US Arizona, US Sydney, AU


If the given prefix is 'A', all cities but Sydney should appear. Contrariwise, if the given prefix is “s”, the only result should be “Sydney, AU”.

If the given prefix is “Al”, “Alabama, US” and “Albuquerque, US” are the only results.

If the prefix given is “Alb” then the only result is “Albuquerque, US”

### Resolution & Business Logic

Please check the DataManager.java and following methods

```
public synchronized List<City> parseCityJsonArray(InputStream inputStream)

public void sortCityListAscending()

public void indexCityList()

public List<City> filterList(String searchText)
```


