package com.backbase.assignment.bbandroidassignment.screens.loading;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;

import com.backbase.assignment.bbandroidassignment.R;
import com.backbase.assignment.bbandroidassignment.core.AppConstants;
import com.backbase.assignment.bbandroidassignment.core.data.DataManager;
import com.backbase.assignment.bbandroidassignment.model.City;
import com.backbase.assignment.bbandroidassignment.utils.Logger;

import java.util.List;

/**
 * Service use to parse json data in background thread. So UI thread should not on load.
 * <p>
 * Created by sachin on 09/11/17.
 */


public class DataParsingService extends IntentService {

    DataManager dataManager;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     */
    public DataParsingService() {
        super(DataParsingService.class.getSimpleName());

        /* Object is already initialize in {@link com.backbase.assignment.bbandroidassignment.BBAssignmentApp} class
         */
        dataManager = DataManager.getInstance();
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {


        Logger.info("DataLoading service Started");

        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.DATA_MESSAGE, getString(R.string.loading_progress_parsing_data));
        sendProgressBrodcast(bundle);
        Logger.info("Json data parsing started");

        List<City> cityList = dataManager.parseCityJsonArray(dataManager.readAssetsFile(AppConstants.JSON_DATA_FILE));
        if (cityList == null) {
            sendBroadcast(AppConstants.ACTION_NOTIFICATION_DATA_LOADING_FAIL, null);
            return;
        }


        Logger.info("Json data sorting started");
        bundle.putString(AppConstants.DATA_MESSAGE, getString(R.string.loading_progress_sorting_city));
        sendProgressBrodcast(bundle);
        dataManager.sortCityListAscending();


        Logger.info("Json data indexing started");
        bundle.putString(AppConstants.DATA_MESSAGE, getString(R.string.loading_progress_indexing));
        sendProgressBrodcast(bundle);
        dataManager.indexCityList();


        Logger.info("Json data parsing, sorting & indexing done");
        sendBroadcast(AppConstants.ACTION_NOTIFICATION_DATA_LOADING_SUCCESS, null);


    }


    @Override
    public void onDestroy() {
        Logger.info("DataLoading service Stopped");
        super.onDestroy();
    }


    private void sendBroadcast(String action, Bundle data) {
        Intent notificationIntent = new Intent();
        notificationIntent.setAction(action);

        if (data != null)
            notificationIntent.putExtras(data);

        LocalBroadcastManager.getInstance(this).sendBroadcast(notificationIntent);
    }


    private void sendProgressBrodcast(Bundle data) {
        sendBroadcast(AppConstants.ACTION_NOTIFICATION_DATA_LOADING_PROGRESS, data);
    }
}
