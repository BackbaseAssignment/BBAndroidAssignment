package com.backbase.assignment.bbandroidassignment.screens.loading;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.backbase.assignment.bbandroidassignment.R;
import com.backbase.assignment.bbandroidassignment.core.AppConstants;
import com.backbase.assignment.bbandroidassignment.core.data.DataManager;
import com.backbase.assignment.bbandroidassignment.core.view.BaseActivity;
import com.backbase.assignment.bbandroidassignment.screens.list.CityListActivity;
import com.backbase.assignment.bbandroidassignment.utils.Logger;

public class LoadingActivity extends BaseActivity implements LoadingContract.View {


    LoadingContract.Presenter mLandingPresenter;
    ProgressBar viewProgressBar;
    TextView viewTextBanner;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);
        // keeping screen on while loading data.
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);


        initViews();

        new LandingPresenterImpl(this);

        // registering broadcast receiver actions
        IntentFilter filter = new IntentFilter();
        filter.addAction(AppConstants.ACTION_NOTIFICATION_DATA_LOADING_SUCCESS);
        filter.addAction(AppConstants.ACTION_NOTIFICATION_DATA_LOADING_PROGRESS);
        filter.addAction(AppConstants.ACTION_NOTIFICATION_DATA_LOADING_FAIL);

        LocalBroadcastManager.getInstance(this).registerReceiver(dataLoadingReceiver, filter);


    }

    private void initViews() {

        Logger.info("LoadingScreen Views Initialization Process Started.");

        viewProgressBar = _findViewById(R.id.pb_loading);
        viewTextBanner = _findViewById(R.id.tv_loading_banner);


        Logger.info("LoadingScreen Views Initialization Process Finished.");
    }


    @Override
    public void startDataLoadingService() {
        mLandingPresenter.loadingStarted(getString(R.string.msg_data_loading_started));
        startService(new Intent(this, DataParsingService.class));
    }


    @Override
    public void setPresenter(LoadingContract.Presenter presenter) {
        Logger.info("Received LoadingPresenter back to LoadingActivity.");
        this.mLandingPresenter = presenter;
    }

    @Override
    protected void onStop() {
        super.onStop();
        this.mLandingPresenter.stop();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Logger.info("LoadingPresenter start() called");

        this.mLandingPresenter.start();
    }

    @Override
    public void showLoadingIndication(boolean isShow) {
        viewProgressBar.setVisibility(isShow ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setBannerText(String message) {
        viewTextBanner.setText(String.format("%s%s", message, getString(R.string.please_wait_message)));
    }

    @Override
    public void navigateToCityListScreen() {
        Logger.info("Assuming... Data loading done successfully so navigation to CityListScreen");
        Intent intent = new Intent(this, CityListActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public String getDataLoadingStartedMessage() {
        return getString(R.string.msg_data_loading_started);
    }


    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(dataLoadingReceiver);
        super.onDestroy();

    }

    BroadcastReceiver dataLoadingReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(AppConstants.ACTION_NOTIFICATION_DATA_LOADING_PROGRESS)) {

                Bundle bundle = intent.getExtras();
                String message = getString(R.string.msg_data_loading_started);
                if (bundle != null) {
                    message = bundle.getString(AppConstants.DATA_MESSAGE, message);
                }
                mLandingPresenter.loadingStarted(message);


            } else if (intent.getAction().equals(AppConstants.ACTION_NOTIFICATION_DATA_LOADING_SUCCESS)) {
                mLandingPresenter.loadingFinished(DataManager.getInstance().getCities());
            } else {
                mLandingPresenter.loadingError(getString(R.string.error_something_went_wrong));
            }

        }
    };


}
