package com.backbase.assignment.bbandroidassignment.screens.list.adapter;

import android.widget.Filter;

import com.backbase.assignment.bbandroidassignment.core.data.DataManager;
import com.backbase.assignment.bbandroidassignment.model.City;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sachin on 09/11/17.
 */

class CityNamePrefixFilter extends Filter {
    private CityListAdapter mAdapter;
    private List<City> cityList;

    public CityNamePrefixFilter(CityListAdapter adapter, List<City> cityList) {
        this.mAdapter = adapter;

        if (cityList == null)
            cityList = new ArrayList<>();
        this.cityList = cityList;

    }

    @Override
    protected FilterResults performFiltering(CharSequence searchText) {

        FilterResults filterResults = new FilterResults();

        if (searchText != null || searchText.length() != 0) {

            searchText = searchText.toString().toUpperCase();

            List<City> filteredCityList = DataManager.getInstance().filterList(searchText.toString());


            filterResults.count = filteredCityList.size();
            filterResults.values = filteredCityList;


        } else {
            filterResults.count = cityList.size();
            filterResults.values = cityList;
        }


        return filterResults;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {


        mAdapter.setData((List<City>) results.values);
        mAdapter.notifyDataSetChanged();
    }
}
