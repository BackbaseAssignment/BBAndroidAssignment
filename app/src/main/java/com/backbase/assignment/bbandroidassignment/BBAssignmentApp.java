package com.backbase.assignment.bbandroidassignment;

import android.app.Application;

import com.backbase.assignment.bbandroidassignment.core.data.DataManager;

/**
 * Created by sachin on 08/11/17.
 */

public class BBAssignmentApp extends Application {


    @Override
    public void onCreate() {
        super.onCreate();


        //Initialization for DataManager by providing Application Context
        DataManager.init(this);


    }
}
