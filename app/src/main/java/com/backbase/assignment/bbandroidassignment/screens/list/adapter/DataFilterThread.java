package com.backbase.assignment.bbandroidassignment.screens.list.adapter;

import com.backbase.assignment.bbandroidassignment.core.data.DataManager;

/**
 * Created by sachin on 09/11/17.
 */

public class DataFilterThread implements Runnable {

    private String searchText;

    public DataFilterThread(String searchText) {

        this.searchText = searchText;
    }


    @Override
    public void run() {
        DataManager.getInstance().filterList(searchText);
    }
}
