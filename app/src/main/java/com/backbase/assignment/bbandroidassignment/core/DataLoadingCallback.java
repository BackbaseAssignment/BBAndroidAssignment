package com.backbase.assignment.bbandroidassignment.core;

/**
 * Created by sachin on 08/11/17.
 */

public interface DataLoadingCallback<T> {

    void loadingStarted();

    void loadingFinished(T t);

    void loadingError(String message);
}
