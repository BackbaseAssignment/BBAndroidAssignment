package com.backbase.assignment.bbandroidassignment.screens.list;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.backbase.assignment.bbandroidassignment.R;
import com.backbase.assignment.bbandroidassignment.core.AppConstants;
import com.backbase.assignment.bbandroidassignment.core.data.DataManager;
import com.backbase.assignment.bbandroidassignment.core.view.BaseActivity;
import com.backbase.assignment.bbandroidassignment.model.City;
import com.backbase.assignment.bbandroidassignment.screens.list.adapter.CityListAdapter;
import com.backbase.assignment.bbandroidassignment.screens.maps.MapsActivity;
import com.backbase.assignment.bbandroidassignment.utils.Logger;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class CityListActivity extends BaseActivity implements CityListContract.View, android.support.v7.widget.SearchView.OnQueryTextListener{


    android.support.v7.widget.SearchView viewSearchField;
    RecyclerView viewCityListRecyclerView;

    TextView viewCityCount;

    CityListAdapter mAdapter;

    CityListContract.Presenter presenter;
    View viewEmpty;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city_list_acitivity);

        initViews();
        new CityListPresenterImpl(this);
        initAdapters();


    }

    private void initAdapters() {
        mAdapter = new CityListAdapter(DataManager.getInstance().getCities(), presenter.getCityItemClickListener());

        mAdapter.registerAdapterDataObserver(adapterDataObserver);

        viewCityListRecyclerView.setAdapter(mAdapter);
    }

    private void initViews() {
        Logger.info("CityListActivity views are initializing");


        viewEmpty = _findViewById(R.id.container_empty_view);

        viewSearchField = _findViewById(R.id.et_search_field);
        viewSearchField.setOnQueryTextListener(this);

        viewCityListRecyclerView = _findViewById(R.id.rv_city_list);
        viewCityListRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        viewCityCount = _findViewById(R.id.tv_total_records);


    }


    @Override
    protected void onStart() {
        super.onStart();
        presenter.start();
    }


    RecyclerView.AdapterDataObserver adapterDataObserver = new RecyclerView.AdapterDataObserver() {
        @Override
        public void onChanged() {
            presenter.validateNoCity(mAdapter.getItemCount());
        }

        @Override
        public void onItemRangeInserted(int positionStart, int itemCount) {
            presenter.validateNoCity(mAdapter.getItemCount());

        }

        @Override
        public void onItemRangeRemoved(int positionStart, int itemCount) {
            presenter.validateNoCity(mAdapter.getItemCount());

        }
    };


    @Override
    public void setPresenter(CityListContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void showEmptyView(boolean isShow) {

        viewEmpty.setVisibility(isShow ? VISIBLE : GONE);


    }

    @Override
    public void showCityMap(City city) {
        Intent intent = new Intent(this, MapsActivity.class);
        intent.putExtra(AppConstants.EXTRA_CITY, city);
        startActivity(intent);
    }




    @Override
    public CityListAdapter getAdapter() {
        return mAdapter;
    }

    @Override
    public void showTotalRecordFounds(int count) {
        viewCityCount.setText(getResources().getQuantityString(R.plurals.number_of_records, count, count));
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String searchText) {
        presenter.filterList(searchText);
        return false;
    }
}
