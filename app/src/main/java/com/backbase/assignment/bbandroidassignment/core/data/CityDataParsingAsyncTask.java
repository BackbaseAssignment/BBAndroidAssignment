package com.backbase.assignment.bbandroidassignment.core.data;

import android.os.AsyncTask;

import com.backbase.assignment.bbandroidassignment.core.AppConstants;
import com.backbase.assignment.bbandroidassignment.core.DataLoadingCallback;
import com.backbase.assignment.bbandroidassignment.model.City;
import com.backbase.assignment.bbandroidassignment.utils.Logger;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sachin on 08/11/17.
 */

public class CityDataParsingAsyncTask extends AsyncTask<InputStream, Void, List<City>> {


    private DataLoadingCallback<List<City>> dataLoadingCallback;


    public CityDataParsingAsyncTask(DataLoadingCallback<List<City>> dataLoadingCallback) {
        this.dataLoadingCallback = dataLoadingCallback;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        if (dataLoadingCallback == null) {
            throw new NullPointerException("DataLoading callback should not be null");
        }


        dataLoadingCallback.loadingStarted();
    }

    @Override
    protected List<City> doInBackground(InputStream... inputStreams) {

        List<City> cityList = null;
        try {

            if (inputStreams == null || inputStreams.length == 0)
                return null;
            Gson gson = new Gson();
            Reader dataReader = new InputStreamReader(inputStreams[0], "UTF-8");


            JsonReader reader = new JsonReader(dataReader);
            cityList = new ArrayList<>();
            reader.beginArray();

            while (reader.hasNext()) {
                City city = gson.fromJson(reader, City.class);
                cityList.add(city);
            }
            reader.endArray();
            reader.close();


            //cityList = Arrays.asList(gson.fromJson(dataReader, City[].class));


            Logger.info("Data Parsing done. Total Records Found: " + cityList.size());
        } catch (Exception e) {
            Logger.error(AppConstants.ERROR_JSON_DATA, e);
        }


        return cityList;
    }


    @Override
    protected void onPostExecute(List<City> cities) {
        super.onPostExecute(cities);

        if (cities == null) {
            dataLoadingCallback.loadingError(AppConstants.ERROR_JSON_DATA);
        } else {
            dataLoadingCallback.loadingFinished(cities);
        }
    }


}
