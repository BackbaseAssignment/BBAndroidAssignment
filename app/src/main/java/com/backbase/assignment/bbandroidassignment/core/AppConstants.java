package com.backbase.assignment.bbandroidassignment.core;

/**
 * Created by sachin on 08/11/17.
 */

public interface AppConstants {

    String JSON_DATA_FILE = "cities.json";

    String ERROR_DATA_LOADING_CANCELLED = "Data loading cancelled.";
    String ERROR_JSON_DATA = "Data parsing error. This error may occurred due to invalid json format or structure.";
    String ERROR_SOMETHING_WENT_WRONG = "OOPS!!! Something went wrong.";


    String EXTRA_CITY = "extra_city";
    String ACTION_NOTIFICATION_DATA_LOADING_SUCCESS = "action_notification_data_loading_success";
    String ACTION_NOTIFICATION_DATA_LOADING_FAIL = "action_notification_data_loading_fail";
    String ACTION_NOTIFICATION_DATA_LOADING_PROGRESS = "action_notification_data_loading_progress";
    String DATA_MESSAGE = "data_message";
}
