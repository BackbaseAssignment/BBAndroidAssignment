package com.backbase.assignment.bbandroidassignment.screens.loading;

import com.backbase.assignment.bbandroidassignment.core.data.CityDataParsingAsyncTask;
import com.backbase.assignment.bbandroidassignment.core.data.DataManager;
import com.backbase.assignment.bbandroidassignment.model.City;

import java.util.List;

/**
 * Created by sachin on 08/11/17.
 */

public class LandingPresenterImpl implements LoadingContract.Presenter {

    LoadingContract.View mLandingView;
    CityDataParsingAsyncTask dataParsingAsyncTask;

    DataManager mDataManager;


    boolean dataLoadingServiceRunning;


    public LandingPresenterImpl(LoadingContract.View landingView) {
        this.mLandingView = landingView;
        mDataManager = DataManager.getInstance();

        mLandingView.setPresenter(this);
    }


    @Override
    public void start() {

//        if (dataParsingAsyncTask.getStatus() == AsyncTask.Status.RUNNING)
//            return;
//
//        dataParsingAsyncTask.execute(mDataManager.readAssetsFile(AppConstants.JSON_DATA_FILE));
//
//
//        DataManager.getInstance().parseCityJsonArray(mDataManager.readAssetsFile(AppConstants.JSON_DATA_FILE));


        if (!dataLoadingServiceRunning) {
            mLandingView.startDataLoadingService();
        }


    }

    @Override
    public void stop() {

    }


    @Override
    public void loadingStarted(String string) {
        dataLoadingServiceRunning = true;
        mLandingView.showLoadingIndication(true);
        mLandingView.setBannerText(string);
    }

    @Override
    public void loadingFinished(List<City> cities) {
        dataLoadingServiceRunning = false;
        mDataManager.setCities(cities);
        mLandingView.navigateToCityListScreen();
    }

    @Override
    public void loadingError(String message) {
        dataLoadingServiceRunning = false;
        mLandingView.setBannerText(message);
    }


}
