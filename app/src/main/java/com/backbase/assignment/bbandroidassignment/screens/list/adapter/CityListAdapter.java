package com.backbase.assignment.bbandroidassignment.screens.list.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.backbase.assignment.bbandroidassignment.R;
import com.backbase.assignment.bbandroidassignment.model.City;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sachin on 09/11/17.
 */

public class CityListAdapter extends RecyclerView.Adapter<CityItemViewHolder> implements Filterable {


    private OnCityClickListener onCityClickListener;
    private List<City> cityList;
    private CityNamePrefixFilter cityPrefixNameFilter;


    public CityListAdapter(List<City> cityList, OnCityClickListener onCityClickListener) {
        this.onCityClickListener = onCityClickListener;
        this.cityList = cityList == null ? new ArrayList<City>() : cityList;
    }

    @Override
    public CityItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_city_list, parent, false);
        CityItemViewHolder viewHolder = new CityItemViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CityItemViewHolder holder, int position) {
        holder.bindData(cityList.get(position), position, onCityClickListener);
    }

    @Override
    public int getItemCount() {
        return cityList.size();
    }


    @Override
    public Filter getFilter() {

        if (cityPrefixNameFilter == null) {
            cityPrefixNameFilter = new CityNamePrefixFilter(this, cityList);

        }


        return cityPrefixNameFilter;
    }

    public void setData(List<City> cityList) {
        this.cityList = cityList;
    }
}
