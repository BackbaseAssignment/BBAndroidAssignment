package com.backbase.assignment.bbandroidassignment.screens.list.adapter;

import com.backbase.assignment.bbandroidassignment.model.City;

/**
 * Created by sachin on 09/11/17.
 */

public interface OnCityClickListener {

    void onCityClick(City city, int position);

}
