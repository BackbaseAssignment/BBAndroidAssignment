package com.backbase.assignment.bbandroidassignment.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sachin on 08/11/17.
 */

public class City implements Parcelable {

    @SerializedName("country")
    String country;

    @SerializedName("name")
    String name;

    @SerializedName("_id")
    int id;


    @SerializedName("coord")
    Coordinate coord;


    public City() {
    }

    protected City(Parcel in) {
        country = in.readString();
        name = in.readString();
        id = in.readInt();
        coord = in.readParcelable(Coordinate.class.getClassLoader());
    }

    public static final Creator<City> CREATOR = new Creator<City>() {
        @Override
        public City createFromParcel(Parcel in) {
            return new City(in);
        }

        @Override
        public City[] newArray(int size) {
            return new City[size];
        }
    };

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Coordinate getCoord() {
        return coord;
    }

    public void setCoord(Coordinate coord) {
        this.coord = coord;
    }


    @Override
    public boolean equals(Object obj) {

        if (obj instanceof City)
            return this.id == ((City) obj).getId();
        else
            return false;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(country);
        dest.writeString(name);
        dest.writeInt(id);
        dest.writeParcelable(coord, flags);
    }
}
