package com.backbase.assignment.bbandroidassignment.core;

/**
 * Created by sachin on 08/11/17.
 */

public interface IPresenter {


    /**
     * Method should perform operation based on screen onStart lifecycle called. <br>
     * Need to be careful if long running operation called then should have check for isRunning.
     * If anything is already running then should not start the respective operation
     */
    void start();

    /**
     * method should perform operation based on screen stop.
     */
    void stop();
}
