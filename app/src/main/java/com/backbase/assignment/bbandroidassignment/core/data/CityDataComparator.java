package com.backbase.assignment.bbandroidassignment.core.data;

import com.backbase.assignment.bbandroidassignment.model.City;

/**
 * Created by sachin on 08/11/17.
 */

class CityDataComparator implements java.util.Comparator<City> {

    @Override
    public int compare(City city1, City city2) {

        return city1.getName().toUpperCase().compareTo(city2.getName().toUpperCase());
    }
}
