package com.backbase.assignment.bbandroidassignment.core.data;

import android.annotation.SuppressLint;
import android.content.Context;

import com.backbase.assignment.bbandroidassignment.core.AppConstants;
import com.backbase.assignment.bbandroidassignment.model.City;
import com.backbase.assignment.bbandroidassignment.utils.Logger;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * This class used to handle city related data operations like parsing, sorting, filtering etc.
 * <p>
 * Created by sachin on 08/11/17.
 */

public class DataManager {


    private static DataManager mInstance;


    private List<City> cities = new ArrayList<>();
    ;

    private Context context;

    private ConcurrentHashMap<String, String> alphabetPositionMap = new ConcurrentHashMap<>();


    private DataManager(Context context) {
        this.context = context;
    }

    public static synchronized void init(Context context) {
        if (context == null) {
            throw new NullPointerException("While DataManager initialization, Context should not be null");
        }

        if (mInstance == null)
            mInstance = new DataManager(context);
    }

    public static DataManager getInstance() {
        if (mInstance == null) {
            throw new RuntimeException("DataManager not initialized. Please call DataManager.init(Context) before using any functionality");
        }

        return mInstance;
    }


    /**
     * Read file from assets by giving file path or file name if it is in the assets (root folder)
     *
     * @param fileName
     * @return {@link InputStream} if file available else null.
     */
    public InputStream readAssetsFile(String fileName) {

        try {

            Logger.info("Reading file \"" + fileName + "\" from assets folder");
            return context.getAssets().open(fileName);
        } catch (IOException e) {
            Logger.error("Error while reading file \"" + fileName + "\" from assets folder");
        }
        return null;
    }

    /**
     * get cities list
     *
     * @return {@link List<City>} can be empty but not null.
     */
    public List<City> getCities() {
        return cities;
    }


    /**
     * Set city list to DataManager
     *
     * @param cities
     */
    public synchronized void setCities(List<City> cities) {

        if (cities == null)
            cities = new ArrayList<>();

        this.cities = cities;
    }


    /**
     * Parsing JSON for array of {@link City} object
     *
     * @param inputStream
     * @return NULL if null or unformatted / unexpected parameter passed <br>OR<br>{@link List<City>} can be empty.
     */
    public synchronized List<City> parseCityJsonArray(InputStream inputStream) {
        try {

            if (inputStream == null)
                return null;

            Gson gson = new GsonBuilder().create();
            Reader dataReader = new InputStreamReader(inputStream, "UTF-8");
            JsonReader reader = new JsonReader(dataReader);

            reader.beginArray();
            cities.clear();
            while (reader.hasNext()) {
                City city = gson.fromJson(reader, City.class);
                cities.add(city);
            }
            reader.endArray();
            reader.close();

            Logger.info("Data Parsing done. Total Records Found: " + cities.size());
        } catch (Exception e) {

            e.printStackTrace();
            Logger.error(AppConstants.ERROR_JSON_DATA, e);
            return null;
        }


        return cities;
    }


    @SuppressLint("DefaultLocale")
    public void sortCityListAscending() {

        // Sorting cities by ascending order
        Collections.sort(cities, new CityDataComparator());


    }

    /**
     * Indexing city list for fast searching / filtering.<br>
     * Indexing based on First Character of each city. After sorting it in ascending order there will be a fix list index range for respective alphabets.<br>
     * So while searching / filtering, we can directly start searching from that respective index to its range.<br>
     * This process may take some time but will help for further processing.
     * <br><br>
     * Indexing stored in {@link java.util.HashMap} where as key is alphabet / first character of city. Whereas startIndex and endIndex concatenate by (-) hyphen. So whenever filtering starts it will get the first character of search string, get the value of that character from hashmap, split it by (-) hyphen and we can get start and end range of that expected result.
     */
    @SuppressLint("DefaultLocale")
    public void indexCityList() {


        int totalCities = cities.size();
        String previousSectionStartsWith = "";
        int previousSectionIndex = 0;
        for (int index = 0; index < totalCities; index++) {

            String section = cities.get(index).getName().toUpperCase().substring(0, 1);


            if (!alphabetPositionMap.containsKey(section)) {


                if (previousSectionStartsWith.length() != 0) {

                    // updating previous index range based on new range started from this index.

                    alphabetPositionMap.put(previousSectionStartsWith, previousSectionIndex + "-" + (index - 1));
                }
                alphabetPositionMap.put(section, index + "-" + (totalCities - 1));

                previousSectionStartsWith = section;
                previousSectionIndex = index;

            }
        }
    }

    /**
     * @param searchText City name / prefix to search city from list.
     * @return can be empty city list
     */
    public List<City> filterList(String searchText) {

        if (searchText == null || searchText.length() == 0)
            return cities;

        List<City> filteredCityList = new ArrayList<>();


        String indexRange = alphabetPositionMap.get(searchText.toUpperCase().substring(0, 1));
        searchText = searchText.toUpperCase();
        if (indexRange != null) {

            String[] indexes = indexRange.split("-");

            int startIndex = Integer.parseInt(indexes[0]);
            int endIndex = Integer.parseInt(indexes[1]);

            // added to avoid looping.
            // We have already sorted list. if after finding any record next record not match with search criteria then no need to loop future. Therefore added flag.
            boolean isResultFound = false;


            for (; startIndex <= endIndex; startIndex++) {
                City city = cities.get(startIndex);
                if (city.getName().toUpperCase().startsWith(searchText)) {
                    filteredCityList.add(city);
                    isResultFound = true;
                } else if (isResultFound) {
                    break;
                }
            }
        }
        return filteredCityList;
    }

    public String getIndexRange(String suffixCharacter) {
        return alphabetPositionMap.get(suffixCharacter);
    }
}

