package com.backbase.assignment.bbandroidassignment.screens.list;

import com.backbase.assignment.bbandroidassignment.core.IBaseView;
import com.backbase.assignment.bbandroidassignment.core.IPresenter;
import com.backbase.assignment.bbandroidassignment.model.City;
import com.backbase.assignment.bbandroidassignment.screens.list.adapter.CityListAdapter;
import com.backbase.assignment.bbandroidassignment.screens.list.adapter.OnCityClickListener;

/**
 * Created by sachin on 08/11/17.
 */

public interface CityListContract {

    interface View extends IBaseView<Presenter> {

        void showEmptyView(boolean b);

        void showCityMap(City city);



        CityListAdapter getAdapter();

        void showTotalRecordFounds(int count);
    }


    interface Presenter extends IPresenter {

        OnCityClickListener getCityItemClickListener();

        void validateNoCity(int dataCount);



        void filterList(String s);
    }
}
