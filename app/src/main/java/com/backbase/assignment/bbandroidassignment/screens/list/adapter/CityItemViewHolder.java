package com.backbase.assignment.bbandroidassignment.screens.list.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.backbase.assignment.bbandroidassignment.R;
import com.backbase.assignment.bbandroidassignment.model.City;

/**
 * Created by sachin on 09/11/17.
 */

public class CityItemViewHolder extends RecyclerView.ViewHolder {


    TextView viewCityName;

    public CityItemViewHolder(View itemView) {
        super(itemView);
        viewCityName = itemView.findViewById(R.id.tv_item_city_name);
    }


    public void bindData(final City city, final int position, final OnCityClickListener onCityClickListener) {
        this.itemView.setTag(city);
        this.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onCityClickListener != null)
                    onCityClickListener.onCityClick(city, position);
            }
        });
        this.itemView.setBackgroundResource(position % 2 == 0 ? R.color.row_even : R.color.row_odd);
        viewCityName.setText(String.format("%s, %s", city.getName(), city.getCountry()));
    }
}
