package com.backbase.assignment.bbandroidassignment.screens.loading;

import com.backbase.assignment.bbandroidassignment.core.IBaseView;
import com.backbase.assignment.bbandroidassignment.core.IPresenter;
import com.backbase.assignment.bbandroidassignment.model.City;

import java.util.List;

/**
 * Created by sachin on 08/11/17.
 */

interface LoadingContract {


    interface View extends IBaseView<Presenter> {

        void startDataLoadingService();

        void showLoadingIndication(boolean isShow);

        void setBannerText(String message);

        void navigateToCityListScreen();

        String getDataLoadingStartedMessage();
    }


    interface Presenter extends IPresenter {

        void loadingStarted(String string);

        void loadingFinished(List<City> cities);

        void loadingError(String message);
    }

}
