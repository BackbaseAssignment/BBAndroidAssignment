package com.backbase.assignment.bbandroidassignment.utils;

/**
 * Created by sachin on 08/11/17.
 */

public class Logger {

    static final String TAG = "BBAssignment";
    static final boolean IS_LOG_ENABLE = true;


    public static void info(String logMessage) {
        if (IS_LOG_ENABLE) {
            System.out.println(logMessage);
//            Log.i(TAG, logMessage);
        }
    }


    public static void verbose(String logMessage) {
        if (IS_LOG_ENABLE) {
            System.out.println(logMessage);
//            Log.v(TAG, logMessage);
        }
    }


    public static void warn(String logMessage) {
        if (IS_LOG_ENABLE) {
            System.out.println(logMessage);
//            Log.w(TAG, logMessage);
        }
    }

    public static void debug(String logMessage) {
        if (IS_LOG_ENABLE) {
            System.out.println(logMessage);
//            Log.d(TAG, logMessage);
        }
    }


    public static void error(String logMessage) {
        error(logMessage, null);
    }


    public static void error(String logMessage, Throwable throwable) {

        System.err.println(logMessage);
        if (throwable != null)
            throwable.printStackTrace();

//        Log.e(TAG, logMessage, throwable);
    }


}
