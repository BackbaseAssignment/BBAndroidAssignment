package com.backbase.assignment.bbandroidassignment.core.view;

import android.support.v7.app.AppCompatActivity;
import android.view.View;

/**
 * Created by sachin on 08/11/17.
 */

public class BaseActivity extends AppCompatActivity {


    protected <T extends View> T _findViewById(int resourceId) {
        return (T) findViewById(resourceId);
    }
}
