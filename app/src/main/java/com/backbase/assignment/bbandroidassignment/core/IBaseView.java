package com.backbase.assignment.bbandroidassignment.core;

/**
 * Created by sachin on 08/11/17.
 */

public interface IBaseView<T extends IPresenter> {


    /**
     * Whenever presenter instance created, {@link IBaseView#setPresenter(IPresenter)} should be call from constructor.
     * So that activity or implemented class will get to know that, presenter is ready to use.
     *
     * @param presenter
     */
    void setPresenter(T presenter);

}
