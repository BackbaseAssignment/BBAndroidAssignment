package com.backbase.assignment.bbandroidassignment.screens.list;

import android.widget.Filter;

import com.backbase.assignment.bbandroidassignment.core.data.DataManager;
import com.backbase.assignment.bbandroidassignment.model.City;
import com.backbase.assignment.bbandroidassignment.screens.list.adapter.OnCityClickListener;

/**
 * Created by sachin on 08/11/17.
 */

public class CityListPresenterImpl implements CityListContract.Presenter, OnCityClickListener, Filter.FilterListener {


    private CityListContract.View cityListView;
    private DataManager dataManager;

    CityListPresenterImpl(CityListContract.View cityListView) {

        this.cityListView = cityListView;
        dataManager = DataManager.getInstance();
        this.cityListView.setPresenter(this);
    }

    @Override
    public void start() {
        cityListView.showTotalRecordFounds(dataManager.getCities().size());
    }

    @Override
    public void stop() {

    }


    @Override
    public OnCityClickListener getCityItemClickListener() {
        return this;
    }

    @Override
    public void validateNoCity(int dataCount) {

        cityListView.showEmptyView(dataCount == 0);


    }

//    @Override
//    public void onCancelSearchClick() {
//        cityListView.setSearchText(null);
//        cityListView.showSearchCancelIcon(false);
//        cityListView.getAdapter().setData(dataManager.getCities());
//        cityListView.getAdapter().notifyDataSetChanged();
//    }

    @Override
    public void filterList(String s) {
        cityListView.getAdapter().getFilter().filter(s, this);
    }


    @Override
    public void onCityClick(City city, int position) {
        cityListView.showCityMap(city);
    }

    @Override
    public void onFilterComplete(int count) {
        if (count == 0) {
            cityListView.showEmptyView(true);
        } else {
            cityListView.showEmptyView(false);

        }
        cityListView.showTotalRecordFounds(count);
    }
}
