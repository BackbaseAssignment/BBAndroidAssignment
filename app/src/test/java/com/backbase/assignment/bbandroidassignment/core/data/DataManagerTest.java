package com.backbase.assignment.bbandroidassignment.core.data;

import android.content.Context;

import com.backbase.assignment.bbandroidassignment.BBAssignmentApp;
import com.backbase.assignment.bbandroidassignment.model.City;
import com.backbase.assignment.bbandroidassignment.model.Coordinate;

import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by sachin on 10/11/17.
 */
public class DataManagerTest {

    DataManager dataManager;

    @Before
    public void setUp() throws Exception {

        Context context = new BBAssignmentApp();
        DataManager.init(context);
        dataManager = DataManager.getInstance();
        InputStream is = new FileInputStream(Paths.get("").toAbsolutePath().toString() + "/app/src/main/assets/cities.json");
        dataManager.parseCityJsonArray(is);

    }


    @Test
    public void parseCityJsonArray_Validate_Total_Records() throws Exception {
        InputStream is = new FileInputStream(Paths.get("").toAbsolutePath().toString() + "/app/src/main/assets/cities.json");
        List<City> cityList = dataManager.parseCityJsonArray(is);
        int expected = 209557;
        assertEquals("citiesList size", expected, cityList.size());

    }

    @Test
    public void parseCityJsonArray_Invalid_InputStream() throws Exception {
        InputStream is = new FileInputStream(Paths.get("").toAbsolutePath().toString() + "/app/src/main/assets/cities.json");
        List<City> cityList = dataManager.parseCityJsonArray(is);
        int expected = 209557;
        assertEquals("citiesList size", expected, cityList.size());

    }

    @Test
    public void sortCityListAscending_fail() throws Exception {

        List<City> cityListWithExitingOrder = new ArrayList<>(dataManager.getCities());

        City city = new City();
        city.setName("Test Name");
        city.setCountry("Test Country");
        city.setId(0);
        Coordinate coordinate = new Coordinate();
        coordinate.setLatitude(999999.99999);
        coordinate.setLongitude(999999.99999);
        city.setCoord(coordinate);

        cityListWithExitingOrder.add(city);

        dataManager.sortCityListAscending();


        Collections.sort(cityListWithExitingOrder, new CityDataComparator());


        assertNotEquals("Sorterd Order is Matching", cityListWithExitingOrder, dataManager.getCities());


    }

    @Test
    public void sortCityListAscending_success() throws Exception {

        List<City> cityListWithExitingOrder = new ArrayList<>(dataManager.getCities());

        dataManager.sortCityListAscending();


        Collections.sort(cityListWithExitingOrder, new CityDataComparator());


        assertEquals("Sorterd Order is Missmatch", cityListWithExitingOrder, dataManager.getCities());


    }

    @Test
    public void indexCityList() throws Exception {


    }

    @Test
    public void filterList_valid_result() throws Exception {

        dataManager.sortCityListAscending();
        dataManager.indexCityList();
        List<City> result = dataManager.filterList("Delhi");
        int expectedRecords = 7;
        assertEquals("Expected filtered records mismatch", expectedRecords, result.size());

        result = dataManager.filterList("New York");
        expectedRecords = 7;
        assertNotEquals("Expected filtered records match", expectedRecords, result.size());


        result = dataManager.filterList("Pune");
        expectedRecords = 1;
        assertEquals("Expected filtered records not match", expectedRecords, result.size());

    }


    @Test
    public void filterList_Find_Prefix_Range_Success() throws Exception {
        dataManager.sortCityListAscending();
        dataManager.indexCityList();
        List<City> result = dataManager.filterList("P");
        String range = dataManager.getIndexRange("P");
        String[] rangeArray = range.split("-");

        int expectedRecords = Integer.parseInt(rangeArray[1]) - Integer.parseInt(rangeArray[0]) + 1;
        assertEquals("Expected filtered records not match", expectedRecords, result.size());

    }

    @Test
    public void filterList_Find_Prefix_Range_success_for_non_suffix_character() throws Exception {
        dataManager.sortCityListAscending();
        dataManager.indexCityList();
        List<City> result = dataManager.filterList("*");
        String range = dataManager.getIndexRange("*");
        int expectedRecords = 0;
        if (range != null) {


            String[] rangeArray = range.split("-");
            expectedRecords = Integer.parseInt(rangeArray[1]) - Integer.parseInt(rangeArray[0]) + 1;
        }
        assertEquals("Expected filtered records not match", expectedRecords, result.size());

    }

}